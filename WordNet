/* *****************************************************************************
 *  Name:Omar.A.K
 *  Date:
 *  Description:
 **************************************************************************** */

import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.DirectedCycle;
import edu.princeton.cs.algs4.In;

import java.util.ArrayList;
import java.util.HashMap;

public class WordNet {
    private Digraph g;
    private final HashMap<Integer, String> idsynset;
    private final HashMap<String, ArrayList<Integer>> synsetid;
    private SAP sap;

    // constructor takes the name of the two input files
    public WordNet(String synsets, String hypernyms) {

        if (synsets == null || hypernyms == null)
            throw new IllegalArgumentException();
        idsynset = new HashMap<>();
        synsetid = new HashMap<>();
        //read the 2 files int lists
        In synin = new In(synsets);
        In hypin = new In(hypernyms);

        while (!synin.isEmpty()) {
            String[] read = synin.readLine().split(",");
            int id = Integer.parseInt(read[0]);
            String synstr = read[1];
            String[] synset = read[1].split(" ");
            idsynset.put(id, synstr);
            for (String s : synset) {
                if (!synsetid.containsKey(s)) {
                    synsetid.put(s, new ArrayList<>());
                }
                synsetid.get(s).add(id);
            }

        }
        g = new Digraph(idsynset.size());
        //build the graph
        while (!hypin.isEmpty()) {
            //stores hypernyms from index 1
            String[] read1 = hypin.readLine().split(",");
            int id = Integer.parseInt(read1[0]); //syns id
            for (int i = 1; i < read1.length; i++) {
                g.addEdge(id, Integer.parseInt(read1[i]));
            }

        }
        //check if it is DAG or not
        if (new DirectedCycle(g).hasCycle()) {
            throw new IllegalArgumentException("Not a rooted DAG");
        }
        int roots = 0;
        for (int i = 0; i < g.V(); i++)
            if (!g.adj(i).iterator().hasNext())
                roots++;

        if (roots > 1)
            throw new IllegalArgumentException(
                    "Not a rooted DAG");

        sap = new SAP(g);
    }

    // returns all WordNet nouns
    public Iterable<String> nouns() {

        return synsetid.keySet();
    }

    // is the word a WordNet noun?
    public boolean isNoun(String word) {

        if (word == null) {
            throw new IllegalArgumentException();
        }
        return (synsetid.containsKey(word));
    }

    // distance between nounA and nounB (defined below)
    public int distance(String nounA, String nounB) {
        if (nounA == null || nounB == null)
            throw new IllegalArgumentException();
        int ans = sap.length(synsetid.get(nounA), synsetid.get(nounB));
        return ans;
    }

    // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
    // in a shortest ancestral path (defined below)
    public String sap(String nounA, String nounB) {
        if (nounA == null || nounB == null)
            throw new IllegalArgumentException();
        int ans = sap.ancestor(synsetid.get(nounA), synsetid.get(nounB));
        return idsynset.get(ans);
    }

    // do unit testing of this class
    public static void main(String[] args) {
        In in = new In("testing/digraph1.txt");
        Digraph g = new Digraph(in);
        SAP sap = new SAP(g);

        assert sap.length(3, 11) == 4 && sap.ancestor(3, 11) == 1;
        assert sap.length(9, 12) == 3 && sap.ancestor(9, 12) == 5;
        assert sap.length(7, 2) == 4 && sap.ancestor(7, 2) == 0;
        assert sap.length(1, 6) == -1 && sap.ancestor(1, 6) == -1;

        WordNet wordnet = new WordNet("testing/synsets.txt", "testing/hypernyms.txt");
        assert wordnet.sap("Pericallis_cruenta", "Phlox_stellaria")
                      .equals("vascular_plant tracheophyte");

        wordnet = new WordNet("testing/synsets100-subgraph.txt",
                              "testing/hypernyms100-subgraph.txt");
        assert wordnet.sap("IgA", "unit_cell").equals("unit building_block");
        wordnet = new WordNet("testing/synsets500-subgraph.txt",
                              "testing/hypernyms500-subgraph.txt");
        assert wordnet.sap("cyanide_radical", "gelatin").equals("unit building_block");
        wordnet = new WordNet("testing/synsets1000-subgraph.txt",
                              "testing/hypernyms1000-subgraph.txt");
        assert wordnet.sap("sodium_nitrite", "DEAE_cellulose").equals("unit building_block");
    }
}
